<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$app = new \Slim\App;
$app->get('/customers/{number}', function($request, $response,$args){
    $json = '{"1":"Avi", "2":"Moti", "3":"Shira", "4":"Rachel"}';
    $array = (json_decode($json, true));
    if(array_key_exists($args['number'], $array)){
        echo $array[$args['number']];
    }
    else{
        echo "We couldn't find the user";
    }
});
$app->run();